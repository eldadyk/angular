import { RouterModule, Router } from '@angular/router';
import { MessagesService } from './../messages/messages.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {


  invalid = false;
  loginform = new FormGroup({

   user:new FormControl(),
    password:new FormControl()
  });


login(){

this.service.login(this.loginform.value).subscribe(response=>{
 this.router.navigate(['/']); 


}, error=>{
  this.invalid = true;
})


}

logout(){

localStorage.removeItem('token');
this.invalid= false;
}



  constructor(private service:MessagesService, private router:Router) { }

  ngOnInit() {
  }

}
