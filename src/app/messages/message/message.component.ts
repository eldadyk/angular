import { MessagesService } from './../messages.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message;
  constructor(private rout:ActivatedRoute , private service: MessagesService) { 

  

  }
 
  ngOnInit() {
   this.rout.paramMap.subscribe(params=>{
    console.log(params);
    let id = params.get('id');
    console.log(id);
   this.service.getMessage(id).subscribe(response=>{
   this.message = response.json();
   console.log(this.message);
      })

   })

  }

}
