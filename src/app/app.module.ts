import { environment } from './../environments/environment';
import {RouterModule} from '@angular/router'
import { UsersService } from './users/users.service';
import {HttpModule} from '@angular/http'
import { MessagesService } from './messages/messages.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import {  FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { MessageFormComponent } from './messages/messageform/messageform.component';
import { LogInComponent } from './log-in/log-in.component';
import{AngularFireModule } from 'angularfire2';
import{AngularFireDatabaseModule } from 'angularfire2/database';
import { MessagesfComponent } from './messagesf/messagesf.component';


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessagesFormComponent,
    UsersComponent,
    NavigationComponent,
    NotFoundComponent,
    MessageComponent,
    MessageFormComponent,
    LogInComponent,
    MessagesfComponent
  ],
  imports: [
    BrowserModule,
     HttpModule,
     FormsModule,
     ReactiveFormsModule,
     AngularFireModule.initializeApp(environment.firebase),
     AngularFireDatabaseModule,
     RouterModule.forRoot([
       {path:'',component:MessagesComponent},
       {path:'users', component:UsersComponent },
       {path:'message/:id', component:MessageComponent },
       {path: 'messageform/:id', component: MessageFormComponent},
       {path: 'messagesform', component: MessagesFormComponent},
       {path:'log-in', component:LogInComponent },
       {path:'messagesf', component:MessagesfComponent },
       {path:'**', component:NotFoundComponent }

     ])
  ],
  providers: [
    MessagesService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
